import scrapy
from scrapy_selenium import SeleniumRequest
import math
import pandas as pd


class AllhandphonesSpider(scrapy.Spider):
    name = 'allhandphones'
    all_products = []
    allowed_domain = ['tokopedia.com']
    start_urls = f'https://tokopedia.com/p/handphone-tablet/handphone?page=1'

    def start_requests(self):
            yield SeleniumRequest(
                url = self.start_urls,
                callback = self.pagination,
            )

    def pagination(self, response):
        current_load_product = int(response.css('div.css-1dq1dix div div strong::text').getall()[2])
        prev_load_product = int(response.css('div.css-1dq1dix div div strong::text').getall()[0])
        load_product_per_page = current_load_product - prev_load_product

        total_products = int(response.css('div.css-1dq1dix div div strong::text').getall()[5])
        total_pages = math.ceil(total_products/load_product_per_page)
        print(f'Total pages {total_pages}')

        # Change pages = total_pages for scraping all pages.
        pages = 10

        for x in range(1,pages+1):
            yield SeleniumRequest(
                url = f'https://tokopedia.com/p/handphone-tablet/handphone?page={x}',
                script='window.scrollTo(0, document.body.scrollHeight);',
                wait_time= 60,
                dont_filter = True,
                callback = self.parse_products,
            )

    def parse_products(self, response):
        products = response.css('div.css-bk6tzz')
        for p in products:
            item = {
                "linkProduct" : p.css('a::attr(href)').get(),
                "review" : p.css('div.css-153qjw7 div span::text').get()
            }
            self.all_products.append(item)
            yield item
        df = pd.DataFrame(self.all_products)
        df.to_csv('all_products.csv', index=False, encoding="utf-8")
