import scrapy
from scrapy_selenium import SeleniumRequest
import math
import pandas as pd

class Top100handphonesSpider(scrapy.Spider):
    name = 'top100handphones'
    allowed_domain = ['tokopedia.com']
    top100Product = []

    def start_requests(self):
        df = pd.read_csv(r"C:\Users\fitra\Documents\My Gitlab\tokopedia_scraping\all_products.csv")

        df['review'] = df['review'].str.replace('\W', '')
        drop_null = df.drop(df[(df.review == '')].index)
        drop_nan = drop_null[drop_null['review'].notna()]
        drop_nan['review'] = drop_nan['review'].astype(int)
        sorted = drop_nan.sort_values(by=['review'], ascending=False)
        review_top100 = sorted.head(100)

        print(review_top100)                                                                                                                                                                    

        urls = review_top100['linkProduct'].to_list()
        for url_Product in urls:
            yield SeleniumRequest(
                url = url_Product,
                script='window.scrollTo(0, document.body.scrollHeight);',
                wait_time= 60,
                dont_filter = True,
                callback = self.parse_product_detail,
            )

    def parse_product_detail(self, response):
        details = response.css('#main-pdp-container')
        item = {
            "name" : details.css('h1.css-1wtrxts::text').get(), 
            "desc" : details.css('div.css-1k1relq span span div::text').extract(),
            "imagelink" : details.xpath('//img[@class="success fade"]/@src').get(), 
            "price" : details.css('div.price::text').get(),
            "rating" : details.css('div.css-7fidm1 div.items div span span.main::text').get(),
            "store" : details.css('a.css-1n8curp > h2::text').get(),
            "sold" : details.css('div.css-7fidm1 > div.items > div::text').get()
        }
        self.top100Product.append(item)
        df = pd.DataFrame(self.top100Product)
        df.to_csv('tophundred.csv', index=False, encoding="utf-8")
        yield item
