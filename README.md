# Web Scraping Tokopedia
Scrapy project to scrape tokopedia.com

This is some features of this project
<ul>
  <li>
    To get all of handphones product, use this command<br>
    <b><i>scrapy crawl promo allhandphones</i></b>
    <br><br>
    After get the csv output (filename: all_products.csv, column: produck link and review), you can analyze the result using data science tool like pandas.
  </li>
  <br>
  <li>
    To get Top 100 of handphones product, you can use this command<br>
    <b><i>scrapy crawl top100handphones</i></b>
    <br><br>
    After get the csv output (filename: tophundred.csv, name, desc, imagelink, price, rating, store, sold), you can analyze the result using data science tool like pandas.
  </li>
  <br>
</ul>
